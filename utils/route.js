const ROUTE_PATTERN_REGEX = /(?:{([_a-zA-Z][a-zA-Z0-9_]*)})/gmi;
const PATH_SLASHES_REGEX = /\/+/gmi;

export function parseRoute(pattern, href) {
    const mappings = [];
    const routeRegex = '^' + pattern
        .replace(PATH_SLASHES_REGEX, '\\/')
        .replace(ROUTE_PATTERN_REGEX, (match, routeItemName) => {
            mappings.push({ id: routeItemName });
            return "(.+)";
        }) + '$';

    const match = new RegExp(routeRegex).exec(href);
    if (!match) {
        return [];
    }

    match.shift();

    return match.reduce((carry, routeParameterValue, routeParameterIndex) => {
        const routeParameterInfo = mappings[routeParameterIndex];
        carry[routeParameterInfo.id] = routeParameterValue;

        return carry;
    }, {});
}

export function parseDocumentRoute(pattern) {
    return parseRoute(pattern, document.location.pathname);
}