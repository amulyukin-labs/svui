const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const SRC_DIR = path.join(__dirname, 'src');
const DIST_DIR = path.join(__dirname, 'dist');

const filesInEntriesFolder = fs.readdirSync(path.join(SRC_DIR, 'entries'))
    .filter(fileName => fileName.endsWith('.js'));

const entryObject = filesInEntriesFolder.reduce((p, fileName) => {
    p[path.basename(fileName, '.js')] = [path.join(SRC_DIR, 'entries', fileName)];
    return p;
}, {});

fs.readdirSync(DIST_DIR)
    .forEach(fileName => {
        fs.unlinkSync(path.join(DIST_DIR, fileName));
    });

module.exports = (env, argv) => {
    const devMode = argv.mode !== 'production';

    return {
        entry: entryObject,
        output: {
            path: DIST_DIR,
            filename: '[name].js',
            chunkFilename: '[name].[id].js'
        },
        module: {
            rules: [
                {
                    test: /\.(gif|png|jpe?g|svg)$/i,
                    use: [
                        'file-loader',
                        {
                            loader: 'image-webpack-loader',
                            options: {
                                bypassOnDebug: true, // webpack@1.x
                                disable: true, // webpack@2.x and newer
                            },
                        },
                    ],
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        'file-loader'
                    ]
                },
                {
                    test: /\.(js)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.(svelte)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'svelte-loader',
                        options: {
                            emitCss: true,
                        },
                    },
                },
                {
                    test: /\.(scss|sass|css)$/,
                    include: /node_modules|main.scss$/,
                    loaders: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: devMode
                            }
                        },
                        'sass-loader'
                    ]
                },
                {
                    test: /\.(scss|sass|css)$/,
                    exclude: /node_modules|main.scss$/,
                    loaders: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                modules: {
                                    localIdentName: '[local]'
                                },
                                sourceMap: devMode,
                                importLoaders: 1,
                            }
                        },
                        'sass-loader',
                    ]
                },
                {
                    test: /\.(html)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'html-loader',
                        options: { minimize: true }
                    }
                }
            ]
        },
        resolve: {
            alias: {
                svelte: path.resolve('node_modules', 'svelte')
            },
            extensions: ['*', '.mjs', '.js', '.svelte'],
            mainFields: ['svelte', 'browser', 'module', 'main']
        },
        devtool: devMode ? false : '(none)',
        plugins: [
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css'
            }),
            ...(devMode
                ? [new webpack.SourceMapDevToolPlugin({
                        filename: '[name].js.map',
                        exclude: [
                            '/node_modules/'
                        ]
                    })]
                : [])
        ]
    };
}