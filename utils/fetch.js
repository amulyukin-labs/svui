const DEFAULT_FETCH_CONFIG = {};

const DEFAULT_FETCH_HEADERS = {
    'Accept': 'application/json'
}

function makeFetch(endpoint, init) {
    return fetch(endpoint, {
        ...DEFAULT_FETCH_CONFIG,
        ...init,
        headers: {
            ...DEFAULT_FETCH_HEADERS,
            ...init.headers
        }
    });
}

function handleResponseJsonOutput(response) {
    return new Promise((resolve, reject) => {
        response.text()
            .then(plain => {
                if (!response.ok) {
                    console.warn('Request failed', plain);
                }

                if (plain == null || plain.length === 0) {
                    return resolve(null);
                }

                return resolve(JSON.parse(plain));
            })
            .catch(reject);
    });
}

export function get(endpoint) {
    return makeFetch(endpoint, {
            method: 'GET',
        })
        .then(response => {
            return handleResponseJsonOutput(response);
        });
}

export function upload(endpoint, fileName, file) {
    const formData = new FormData();
    formData.append(fileName, file);

    return makeFetch(endpoint, {
        method: 'POST',
        body: formData,
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return handleResponseJsonOutput(response);
    });
}

export function post(endpoint, payload) {
    let payloadConfig = {};

    if (payload) {
        payloadConfig = {
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        }
    }

    return makeFetch(endpoint, {
        method: 'POST',
        ...payloadConfig
    }).then(response => {
        return handleResponseJsonOutput(response);
    });
}

export function del(endpoint) {
    return makeFetch(endpoint, {
        method: 'DELETE',
    })
        .then(response => {
            return handleResponseJsonOutput(response);
        });
}