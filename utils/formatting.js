import moment from 'moment';

export const CURRENCY_RUB = '₽';

export function formatShortDate(unixtime) {
    const m = moment.unix(unixtime);

    return m.locale('ru').format("D MMM в HH:mm");
}

export function formatTime(unixtime) {
    const m = moment.unix(unixtime);

    return m.format("HH:mm");
}

export function formatDateTimeFromNow(unixtime) {
    const m = moment.unix(unixtime);

    return m.locale('ru').fromNow();
}

export function formatLongNumbers(value) {
    const parts = [];
    let string = Number.parseInt(value || 0).toString();
    
    while (string.length > 3) {
        parts.push(string.substring(string.length - 3));
        string = string.substring(0, string.length - 3);
    }

    if (string.length > 0) {
        parts.push(string);
    }

    const result = parts.reverse().join(' ');

    return result;
}

export function formatPrice(value, currency, fractionDigits = 2, useSign = false, asHtml = true) {
    const parts = [];
    let string = Number.parseFloat(value || 0).toFixed(fractionDigits).toString();

    let remainingString = '';
    const commaIndex = string.indexOf('.');
    if (commaIndex > 0) {
        remainingString = string.substring(commaIndex).replace('.', ',');
        string = string.substring(0, commaIndex);
    }

    while (string.length > 3) {
        parts.push(string.substring(string.length - 3));
        string = string.substring(0, string.length - 3);
    }

    if (string.length > 0) {
        parts.push(string);
    }

    const sign = useSign ? (value >= 0 ? '+' : '-') : '';

    const result = parts.reverse().join(' ') + remainingString + (currency ? ' ' + currency : '');
    if (asHtml) {
        return sign + result.split(/\s+/i).join('&nbsp;');
    }

    return sign + result;
}