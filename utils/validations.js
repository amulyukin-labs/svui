import { parsePhoneNumberFromString } from 'libphonenumber-js'
import Url from 'url-parse';

const DEFAULT_EMAIL_PATTERN = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/i;
const DEFAULT_URL_PATTERN = /^([^:]+:\/\/)?(\w([\-\w\d])*\.)+(\w){2,}([\/\w\d\-_\.])*(\?[\w\d_\-&=\+\.~@:%~#?]*)?$/iu;

if (!window.Lang) {
    window.Lang = {};
}

export function validateInputs(inputs) {
    const inputValidationResults = inputs.map(input => {
        return input.validate(true);
    });

    return inputValidationResults.every(validationResult => validationResult);
}

export function valueIsNotEmpty(value) {
    if (typeof value !== 'undefined') {
        if (typeof value === 'string') {
            return value && value.length > 0;
        }

        if (typeof value === 'object') {
            return value !== null;
        }

        if (typeof value === 'number') {
            return !isNaN(value);
        }

        return true;
    }

    return false;
}

export function required(options) {
    return (value) => {
        if (valueIsNotEmpty(value)) {
            return undefined;
        }

        return window.Lang.validation_is_required || 'is required';
    };
}

export function requireUsername(options) {
    return (value) => {
        if (value == null) {
            return undefined;
        }

        if(/^[a-z]+[a-z\d_]*$/.test(value)) {
            return undefined;
        }

        return window.Lang.validation_username || 'invalid username';
    };
}

export function requireLatin() {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        if (/^[a-zA-Z]+$/.test(value)) {
            return undefined;
        }

        return "should has only latin symbols";
    }
}

export function requireUrl() {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        if (DEFAULT_URL_PATTERN.test(value)) {
            return undefined;
        }

        return window.Lang.validation_url_require || 'require a valid URL';
    };
}

export function requireUrlProtocol(options) {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }
        
        if (Array.isArray(options)) {
            if (options.some(protocol => value.startsWith(protocol))) {
                return undefined;
            }
        }

        return window.Lang.validation_url_protocol_required || 'require url protocol';
    }
}

export function requireOnlyDomainUrl() {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        const url = new Url(value, true);
        const domainUrl = new Url(`${url.protocol}//${url.host}${value.endsWith('/') ? '/' : ''}`);

        if (url.toString() === domainUrl.toString()) {
            return undefined;
        }

        return window.Lang.validation_url_only_domain || 'url must be link';
    }
}

export function requireUrlName() {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        if (/^[a-zA-Z]+[a-zA-Z\d-]*$/.test(value)) {
            return undefined;
        }

        return window.Lang.validation_url_name || 'must contains only latin';
    }
}

export function requireLength(options) {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        if (options.min) {
            if (value.length < options.min) {
                return window.Lang.validation_length_min ? window.Lang.validation_length_min(options.min) : 'require at least ' + options.min + ' chars';
            }
        }

        if (options.max) {
            if (value.length > options.max) {
                return window.Lang.validation_length_max ? window.Lang.validation_length_max(options.max) : 'max ' + options.max + ' chars';
            }
        }

        return undefined;
    }
}

export function requireEmail() {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        if (!DEFAULT_EMAIL_PATTERN.test(value)) {
            return window.Lang.validation_email || 'must be a valid email';
        }

        return undefined;
    }
}

export function requireEquality(options) {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        if (!options.valueProvider) {
            return undefined;
        }

        const externalValue = options.valueProvider();
        if (externalValue !== value) {
            return window.Lang.validation_equality ? window.Lang.validation_equality(options.fieldName) : 'must be same as ' + options.fieldName;
        }

        return undefined;
    }
}

export function requireRange(options) {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        const parsedValue = Number.parseFloat(value);

        if (options.min) {
            if (parsedValue < options.min) {
                // TODO Replace lang
                return "must be greater than " + (options.min);
            }
        }

        if (options.max) {
            if (parsedValue > options.max) {
                // TODO Replace lang
                return "must be less than " + (options.max);
            }
        }
    }
}

export function requirePhone() {
    return (value) => {
        if (!valueIsNotEmpty(value)) {
            return undefined;
        }

        const phoneNumber = parsePhoneNumberFromString(value);
        if (phoneNumber && phoneNumber.isValid()) {
            return undefined;
        }

        return 'is not phone number'
    }
}