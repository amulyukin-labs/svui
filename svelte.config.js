const svelteSassProcessor = require('./build-utils/svelteSassProcessor');

module.exports = {
    preprocess: {
        style: svelteSassProcessor({
            additionalImports: [
                __dirname.replace(/\\/g, '/') + '/styles/common/index.scss'
            ]
        })
    }
};