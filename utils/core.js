export function runSvelteApp(App) {
    const placeholder = document.getElementById('svelte-placeholder');

    const data = JSON.parse(placeholder.dataset.model || "{}");
    
    const app = new App({
        target: placeholder,
        props: data
    });

    return app;
};
