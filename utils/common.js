export function executeDataProvider(dataProvider, ...args) {
    if (!dataProvider) {
        return [];
    }

    if (Array.isArray(dataProvider)) {
        return Promise.resolve(dataProvider);
    }

    if (typeof(dataProvider) === 'function') {
        return Promise.resolve(dataProvider(...args));
    }

    return Promise.resolve(dataProvider);
}

export function throttling(delay, callback) {
    let timer_id = null;

    return (...args) => {
        if (timer_id) {
            clearTimeout(timer_id);
            timer_id = null;
        }

        timer_id = setTimeout(() => {
            callback(...args);
        }, delay);
    }
}